#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtWebEngineWidgets>
#include <QDebug>

namespace Ui {
class Dialog;
}
class MyCppObj : public QObject{
    Q_OBJECT
public:
    MyCppObj(QObject* p):QObject(p){

    }

public slots:
    void t1(QString msg){
        QMessageBox::warning(NULL,"I am a Qt native MsgBox","Msg: "+msg);
    }
signals:
    void sigNotify(QString msg);
};

class MyWebView : public QWebEngineView{
    Q_OBJECT
public:
    void contextMenuEvent(QContextMenuEvent *event) {
        qDebug()<<"contextMenuEvent:"<<event->globalPos();
        event->accept();
    }
    QWebEngineView *createWindow(QWebEnginePage::WebWindowType type) {
        qDebug()<<"createWindow:"<<type;
        return this;
    }
};

class MyWebPage : public QWebEnginePage{
    Q_OBJECT
public:
    MyWebPage(QWebEngineProfile* p, QObject* p2)
        :QWebEnginePage(p, p2){
    }

public:
    bool acceptNavigationRequest(const QUrl &url, NavigationType type, bool isMainFrame) {
        qDebug()<<Q_FUNC_INFO<<url.toString()<<type<<isMainFrame;
        // 这里拦截 url site 请求
        if( url.toString().contains("qq.com") ){
            return false;
        }
        if( url.toString().contains("360.com") ){
            this->setUrl(QUrl("https://baidu.com"));
            return false;
        }

        return true;
    }
};

class WebUrlRequestInterceptor : public QWebEngineUrlRequestInterceptor
{
    Q_OBJECT
public:
    WebUrlRequestInterceptor(QObject *p = Q_NULLPTR)
        :QWebEngineUrlRequestInterceptor(p){
    }
    void interceptRequest(QWebEngineUrlRequestInfo &info){
        QString rsrct = "";
        switch(info.resourceType()){
            case 0:rsrct="ResourceTypeMainFrame = 0, // top level page";break;
            case 1:rsrct="ResourceTypeSubFrame, // frame or iframe";break;
            case 2:rsrct="ResourceTypeStylesheet, // a CSS stylesheet";break;
            case 3:rsrct="ResourceTypeScript, // an external script";break;
            case 4:rsrct="ResourceTypeImage, // an image (jpg/gif/png/etc)";break;
            case 5:rsrct="ResourceTypeFontResource, // a font";break;
            case 6:rsrct="ResourceTypeSubResource, // an other subresource.";break;
            case 7:rsrct="ResourceTypeObject, // an object (or embed) tag for a plugin,";break;
            case 8:rsrct="ResourceTypeMedia, // a media resource.";break;
            case 9:rsrct="ResourceTypeWorker, // the main resource of a dedicated worker.";break;
            case 10:rsrct="ResourceTypeSharedWorker, // the main resource of a shared worker.";break;
            case 11:rsrct="ResourceTypePrefetch, // an explicitly requested prefetch";break;
            case 12:rsrct="ResourceTypeFavicon, // a favicon";break;
            case 13:rsrct="ResourceTypeXhr, // a XMLHttpRequest";break;
            case 14:rsrct="ResourceTypePing, // a ping request for <a ping>";break;
            case 15:rsrct="ResourceTypeServiceWorker, // the main resource of a service worker.";break;
            case 16:rsrct="ResourceTypeUnknown";break;

            default : rsrct="未知类型";break;
        }
      // 这里拦截 url resource 请求
      qDebug()<<Q_FUNC_INFO<<": "<<info.requestMethod() <<info.requestUrl().toString()<<rsrct;
    }
};

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    MyWebView* m_pView;
    MyCppObj* m_pObj;

private slots:
    void on_btn_go_clicked();

    void urlChanged(const QUrl &url);
    void downloadRequested(QWebEngineDownloadItem *download);
    void on_btn_back_clicked();
    void on_btn_next_clicked();
    void on_btn_go_2_clicked();
    void on_btn_go_3_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
