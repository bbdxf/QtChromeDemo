#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    this->setWindowFlags(this->windowFlags()|Qt::WindowMinMaxButtonsHint);


    auto profile = QWebEngineProfile::defaultProfile();
    QObject::connect(
        profile, SIGNAL(downloadRequested(QWebEngineDownloadItem*)),
        this, SLOT(downloadRequested(QWebEngineDownloadItem*)));

    WebUrlRequestInterceptor *wuri = new WebUrlRequestInterceptor();
    profile->setRequestInterceptor(wuri);

    // SchemeHandler的方法不可用，没效果
//    const QString EXAMPLE_SCHEMA_HANDLER = "tami://";  /* http://, https://, mail:// ....   */
//    const QWebEngineUrlSchemeHandler* installed =  profile->urlSchemeHandler(EXAMPLE_SCHEMA_HANDLER.toUtf8());
//    //if (!installed)
//    {
//        profile->installUrlSchemeHandler(EXAMPLE_SCHEMA_HANDLER.toUtf8(), new ExampleUrlSchemeHandler(this));
//    }
//    qDebug()<<"=========="<<installed<<EXAMPLE_SCHEMA_HANDLER;

    m_pView = new MyWebView();
    m_pView->setMinimumHeight(550);
    ui->verticalLayout_2->addWidget(m_pView,1);


    auto myPage = new MyWebPage(profile, m_pView);
    m_pView->setPage(myPage);

    m_pView->show();

    connect(myPage, SIGNAL(urlChanged(QUrl)), this, SLOT(urlChanged(QUrl)));

    // QObject to js
    // 早期的 WebView，使用 void QWebFrame::addToJavaScriptWindowObject(const QString& name...
    // 新版本建议通过WebChannel来实现双向通信。
    QWebChannel *pWebChannel   = new QWebChannel(m_pView->page());
    m_pObj = new MyCppObj(NULL);
    pWebChannel->registerObject("a1", m_pObj);
    m_pView->page()->setWebChannel(pWebChannel);

    m_pView->setUrl(QUrl::fromLocalFile(QApplication::applicationDirPath()+"/index.html"));

    auto a2 = new QLabel(this);
    a2->setText("<h1>Hello, world!</h1>");
    a2->setGeometry(50,50, 200, 100);
    a2->setStyleSheet("color: red; background-color: rgba(10,10,100,160);"); // 半透明控件
    a2->show();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_btn_go_clicked()
{
    m_pView->setUrl(QUrl(ui->edit_addr->text()));
}

void Dialog::urlChanged(const QUrl &url)
{
    ui->edit_addr->setText(url.toString());
}

void Dialog::downloadRequested(QWebEngineDownloadItem *download)
{
    qDebug()<<download->id()<<download->url()<<download->mimeType();
    QString path = QFileDialog::getSaveFileName(this, tr("Save as"), download->path());
    if (path.isEmpty())
        return;

    download->setPath(path);
    //download->accept();
    download->cancel();
    // 通过Download对象可以获取到下载的进度信息
}

void Dialog::on_btn_back_clicked()
{
    m_pView->triggerPageAction(QWebEnginePage::Back);
}

void Dialog::on_btn_next_clicked()
{
     m_pView->triggerPageAction(QWebEnginePage::Forward);
}

void Dialog::on_btn_go_2_clicked()
{
    m_pView->page()->runJavaScript("showmsg('I come from cpp call js scripts!');");
}

void Dialog::on_btn_go_3_clicked()
{
    emit m_pObj->sigNotify("I come from Cpp call qt sig whcih connect js func! ");
}
